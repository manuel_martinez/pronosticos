module ApplicationHelper
	M = [
			'Ene',
			'Feb',
			'Mar',
			'Abr',
			'May',
			'Jun',
			'Jul',
			'Ago',
			'Sep',
			'Oct',
			'Nov',
			'Dic',			
	]
	def date_format(string)		
		fields = string.split('-')
		return "#{fields[2]}/#{M[fields[1]-1]}/#{fields[0]}"		
	end
	
	def semester_name(start_date,end_date)
		s = ''		
		s = M[start_date.month-1] + ' - ' + M[end_date.month-1] + ' ' + start_date.year.to_s		
		return s
	end
end
