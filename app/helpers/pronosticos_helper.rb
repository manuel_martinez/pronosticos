module PronosticosHelper
  def semestre_text(i)
    [ 
      'Remediales','Primer semestre',
      'Segundo semestre','Tercer semestre',
      'Cuarto semestre','Quinto semestre',
      'Sexto semestre','Septimo semestre',
      'Octavo semestre','Noveno semestre',
      'Decimo semestre'
    ][i]
  end
end
