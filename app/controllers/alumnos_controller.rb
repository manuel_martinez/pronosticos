class AlumnosController < ApplicationController
  before_filter :require_admin
	before_filter :set_usuario
  
	ALUMNOS_PER_PAGE = 15
	MATERIAS_PER_PAGE = 12
	
	def reset_predictions
	  @alumno = Alumno.filtro_director(@usuario).find(params[:id])
	  @alumno.reset_predictions_for(next_semester.id)	  
	  flash[:notice] = "El intento se ha borrado"
	  redirect_to :action => :show, :id => @alumno.id
  end
  
  def pronosticos
    @alumno = Alumno.filtro_director(@usuario).find(params[:id])
    @semestre = next_semester
    @semestres = Semestre.order("fecha_inicio DESC")
  end
	
	def index
		get_planes
	end
	
	def list
		require_post do
			params[:page] = params[:page].nil? ? 0 : params[:page].to_i
			alumnos = Alumno.select("usuarios.nombre, usuarios.apellidos, usuarios.username, alumnos.id").joins("JOIN usuarios ON usuarios.id = alumnos.usuario_id").filtro_director(@usuario).limit(ALUMNOS_PER_PAGE).offset(ALUMNOS_PER_PAGE * params[:page]).order(:username)
			total = Alumno.select("COUNT(*) AS num").filtro_director(@usuario)
			
			if params[:plan_id]
			  alumnos = alumnos.where(:plane_id => params[:plan_id])  
			  total = total.where(:plane_id  => params[:plan_id])
	    end				

			render :json => {
				:contents => alumnos,
				:total => total[0][:num],
				:per_page => ALUMNOS_PER_PAGE
			}
		end
	end
	
	def show		
		@alumno = Alumno.select("alumnos.id, alumnos.plane_id, alumnos.usuario_id").filtro_director(@usuario).where(:id => params[:id]).first
		@status = MateriasCursada::STATUS
	end
	
	def list_materias
		require_post do
			params[:page] = params[:page].nil? ? 0 : params[:page].to_i
			materias = Materia.select("materias.id, materias.nombre, materias.clave, planes_materias.semestre").joins("JOIN planes_materias ON planes_materias.materia_id = materias.id").where("planes_materias.plane_id = ?",params[:plan_id]).order("planes_materias.semestre")
			render :json => {
				:contents => materias.each do |materia|	
					s = MateriasCursada.select("materias_cursadas.id, materias_cursadas.status").where(:alumno_id => params[:alumno_id], :materia_id => materia.id)				 
					materia[:status_materia] = s[0][:status] unless s.empty?
					materia[:materia_cursada_id] = s[0][:id] unless s.empty?
				end,
				:total => Materia.select("COUNT(*) AS num").joins("JOIN planes_materias ON planes_materias.materia_id = materias.id").where("planes_materias.plane_id = ?",params[:plan_id])[0][:num],
				:per_page => MATERIAS_PER_PAGE
			}
		end
	end
	
	def list_pronosticos
    require_post do
      semestre_id = params[:semestre_id].blank? ? next_semester.id : params[:semestre_id].to_i
      @alumno = Alumno.filtro_director(@usuario).find(params[:id])
			if @alumno.programa_internacional_by_semestre_id(semestre_id)
				@contents = []
				@total = 1
			else
				@contents = @alumno.predictions_by_semestre_id(semestre_id)
				@total = @contents.length
			end

			render :json => {
				:contents => @contents,
			  :total    => @total,
			  :per_page => MATERIAS_PER_PAGE
			}
    end  
  end
	
	
	def new
		get_planes
		get_semestres_regla
		@usuario = Usuario.new
	end
	
	def create
		require_post do
			puts params.inspect
			usuario = Usuario.new(params[:usuario])
			usuario.is_admin = false
			if usuario.save
				alumno = Alumno.new(params[:alumno])
				alumno.usuario_id = usuario.id
				alumno.save
				render :json => alumno.errors
			else
				render :json => usuario.errors
			end
		end
	end
	
	def edit
		get_planes
		get_semestres_regla
		@alumno = Usuario.select("usuarios.nombre, usuarios.apellidos, usuarios.username, alumnos.id, alumnos.plane_id, alumnos.semestres_regla").joins("JOIN alumnos ON usuarios.id = alumnos.usuario_id").where("alumnos.id = ?",params[:id]).first
		if @usuario.is_director? && Alumno.where(:id => @alumno.id).where("plane_id IN (?)", Plane.por_director(@usuario.id)).empty?
			redirect_to login_url
		end	
	end
	
	def query
	  require_post do 
	    alumnos = Alumno.select("usuarios.nombre, usuarios.apellidos, usuarios.username, alumnos.id").joins("JOIN usuarios ON usuarios.id = alumnos.usuario_id").filtro_director(@usuario).order(:username)
	    alumnos = alumnos.where("usuarios.username LIKE ? OR usuarios.nombre LIKE ? OR usuarios.apellidos LIKE ?", "%#{params[:query]}%", "%#{params[:query]}%", "%#{params[:query]}%")
	    render :json => alumnos
	  end
	end
	
	def update
		require_post do
			alumno = Alumno.filtro_director(@usuario).where(:id => params[:id]).first
			if !alumno.nil? && alumno.update_attributes(params[:alumno])
				usuario = Usuario.where(:id => alumno.usuario_id).first
				usuario.update_attributes(params[:usuario])
				render :json => usuario.errors
			else
				render :json => alumno.errors unless alumno.nil?
			end
		end
	end
	
	def download_pending_predictions_csv
    unless params[:semestre_id].blank?
      path = Alumno.make_csv_file(Alumno.without_predictions_for(params[:semestre_id]).filtro_director(@usuario))
      send_file path, :type => 'application/csv'
    end      
  end
  
  def download_incomplete_predictions_csv
    unless params[:semestre_id].blank?
      path = Alumno.make_csv_file(Alumno.with_incomplete_predictions_for(params[:semestre_id]).filtro_director(@usuario))
      send_file path, :type => 'application/csv', :x_sendfile => false
    end      
  end

	def download_international_programs_csv
    unless params[:semestre_id].blank?
      path = Alumno.make_csv_file(Alumno.with_international_program_for(params[:semestre_id]).filtro_director(@usuario))
      send_file path, :type => 'application/csv', :x_sendfile => false
    end
	end

	def download_candidates_csv
    unless params[:semestre_id].blank?
      path = Alumno.make_csv_file(Alumno.candidate_for(params[:semestre_id]).filtro_director(@usuario))
      send_file path, :type => 'application/csv', :x_sendfile => false
    end
	end
	
	def delete
		require_post do
			unless params[:id].nil?
				alumno = Alumno.filtro_director(@usuario).where(:id => params[:id]).first
				unless alumno.nil?
					usuario = Usuario.where(:id => alumno.usuario_id).first
					render :json => (usuario.delete && alumno.delete)				
				end
			end			
		end
	end
	private
    def set_usuario
			@usuario = Usuario.find session[:user_id] 
		end

		def get_planes
			usuario = Usuario.find session[:user_id]
			if usuario.is_director?
				@planes = Plane.por_director(usuario.id).map do |id|
					plan = Plane.find(id)
					[plan.nombre, plan.id]
				end
			else			
				@planes = Plane.order(:nombre).map do |plan|
					[plan.nombre, plan.id]
				end
			end
		end

		def get_semestres_regla
			@semestres_regla = []
			for i in 3..9
				@semestres_regla << [i, i-1]
			end
		end
end
