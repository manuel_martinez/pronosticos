class Alumnado::PronosticosController < ApplicationController
  before_filter :require_session
  MAX_MATERIAS = 7 #Cuantas materias puede pronosticar un alumno para un semestre

  def index
    @alumno = Alumno.where(:usuario_id => @user.id).first

    if @alumno.has_predictions_for?(next_semester)
      redirect_to :action => :thank_you and return
    else
      @materias = @alumno.possible_subjects
      @max_items = MAX_MATERIAS
    end
  end

  def thank_you
		@alumno = Alumno.where(:usuario_id => @user.id).first
    @semestre = next_semester
		@content = @alumno.predictions_by_semestre_id(@semestre.id)
		@programa_internacional = @alumno.programa_internacional_by_semestre_id(@semestre.id)
		@candidato = @alumno.candidato_by_semestre_id(@semestre.id)
  end

  def create
    require_post do
      success = false
			@alumno = Alumno.select('id').where(:id => params[:alumno_id]).first
			if @alumno
				@semestre = next_semester
				if params[:programa_internacional] == 'true'
					ProgramaInternacional.create!(:alumno_id => @alumno.id, :semestre_id => @semestre.id)
					success = true
		    elsif params[:checked_items] && params[:checked_items].length <= MAX_MATERIAS
		      params[:checked_items].each do |materia_id|
		        Pronostico.create!(:alumno_id => @alumno.id, :semestre_id => @semestre.id, :materia_id => materia_id)
		      end
					if params[:candidato] == 'true'
						Candidato.create!(:alumno_id => @alumno.id, :semestre_id => @semestre.id)
					end
					success = true
				end
      end
      render :json => {:success => success}
    end
  end

  ##Regresar a administrador
    def changetoadmin
      session[:user_id] = session[:true_user_id]
      session.delete(:true_user_id)
      redirect_to "/admin"
    end


end
