class MateriasCursadasController < ApplicationController
  before_filter :require_admin
	layout Proc.new{|controller| params[:ajax_request].nil? ? :materias : false}

	def new
		unless params[:alumno_id].nil?
			get_status
			@alumno = Alumno.where(:id => params[:alumno_id]).first
		end
	end

	def update
		require_post do
			i = 0
			if params[:status] == "null"
				params[:checked_items].each do |materia_id|
					MateriasCursada.delete_all(:materia_id => materia_id, :alumno_id => params[:alumno_id])
					i += 1
				end
			else
				params[:checked_items].each do |materia_id|
					mc = MateriasCursada.where(:materia_id => materia_id, :alumno_id => params[:alumno_id]).first
					if mc.nil?
						mc = MateriasCursada.new(:alumno_id => params[:alumno_id], :materia_id => materia_id, :status => params[:status])
						mc.save
					else
						mc.update_attributes(:status => params[:status])
					end
					i += 1
				end
			end

			render :json => {:updated => i}
		end
	end

	def import
		if params[:file].nil?
			redirect_to :back, notice: "No se ha cargado el archivo"
			return
		end

		matricula = usuario = alumno_id = ''
		CSV.foreach(params[:file].path, headers: true, return_headers: true) do |row|
			if row.header_row? && !(row.field?('matricula') && row.field?('status') && row.field?('clave'))
        redirect_to :back, notice: "El archivo no tiene las columnas correctas"
        return
      end

			unless row['matricula'] == matricula
				matricula = row['matricula']
				usuario = Usuario.where(:username => row['matricula']).first
				next if usuario.nil?
				alumno_id = usuario.alumno.id
			else
				next if usuario.nil?
			end
			materia = Materia.where(:clave => row['clave']).first
			next if materia.nil?
			if row['status'] != nil
				mc = MateriasCursada.where(:materia_id => materia.id, :alumno_id => alumno_id).first
				if row['status'] =~ /[Cu]/
					status = 0
				elsif row['status'] =~ /[A\d]/
					status = 1
				elsif row['status'] =~ /[PI]/
          status = 2
				else
					status = nil
				end
				if mc.nil?
					mc = MateriasCursada.new(:alumno_id => alumno_id, :materia_id => materia.id, :status => status)
					mc.save
				else
					mc.update_attributes(:status => status)
          mc.save
				end
			else
				MateriasCursada.delete_all(:materia_id => materia.id, :alumno_id => alumno_id)
			end
		end
		redirect_to :back, notice: "Materias importadas"
	end

	def delete
		require_post do
			unless params[:id].nil?
				mc = MateriasCursada.where(:id => params[:id]).first
				render :json => mc.destroy
			end
		end
	end

	private
		def get_status
			@status = MateriasCursada::STATUS
		end
end
