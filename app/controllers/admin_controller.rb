class AdminController < ApplicationController
  before_filter :require_admin, :except => [:login, :logout]
	def index
		@usuario = Usuario.find session[:user_id]
		@semestre = next_semester
		@stats = Pronostico.stats_for_semestre(@semestre.id, @usuario)
		@pending_students = Alumno.without_predictions_for(@semestre.id).filtro_director(@usuario)
	end

	def login
	  @user = Usuario.authenticate params[:login][:username], params[:login][:password]

    if @user.nil?
      json_contents = {:errors => {:login => ['Usuario/Contrase&ntilde;a no v&aacute;lido']}}
    else
      session[:user_id] = @user.id
      if @user.is_admin?
        json_contents = {:url => url_for(:controller => :admin, :action => :index) }
	  	else
        json_contents = {:url => alumnado_pronosticos_url }
      end
    end
    render :json => json_contents
  end

  def logout
    reset_session
    flash[:notice] = "Tu sesion ha terminado"
    redirect_to login_url
  end
##Convertirse en alumno
  def changetoalumno
    @alumno = Alumno.select("alumnos.id, alumnos.plane_id, alumnos.usuario_id").where(:id => params[:id]).first
    session[:true_user_id] = session[:user_id]
    session[:user_id] = @alumno.usuario_id
    redirect_to alumnado_pronosticos_path
  end
##Regresar a administrador, esta función se paso a pronosticos controller
  def changetoadmin
    session[:user_id] = session[:true_user_id]
    session.delete(:true_user_id)
    redirect_to "/admin"
  end

end
