class PlanesController < ApplicationController
  before_filter :require_admin
	layout Proc.new{|controller| params[:ajax_request].nil? ? :planes : false}	
	PLANES_PER_PAGE = 10
	MATERIAS_PER_PAGE = 12
	def index
		
	end
	
	def list
		require_post do
			params[:page] = params[:page].nil? ? 0 : params[:page].to_i
			planes = Plane.select("planes.id, planes.nombre, carreras.nombre AS nombre_carrera, (SELECT COUNT(*) FROM alumnos WHERE plane_id = planes.id) AS num_alumnos").joins("JOIN carreras ON carreras.id = planes.carrera_id").limit(PLANES_PER_PAGE).offset(PLANES_PER_PAGE*params[:page]).order(:nombre)
			render :json => {
				:contents => planes,
				:total => Plane.select("COUNT(*) AS num")[0][:num],
				:per_page => PLANES_PER_PAGE
			}
		end
	end
	
	def new
		get_carreras
	end
	
	def create
		require_post do
			plan = Plane.new(params[:plane])
			plan.save
			render :json => plan.errors
		end
	end
	
	def edit
		get_carreras
		@plane = Plane.where(:id => params[:id]).first
	end
	
	def update
		require_post do
			plan = Plane.where(:id => params[:id]).first
			unless plan.nil?
				plan.update_attributes(params[:plane])
				render :json => plan.errors
			end
		end
	end
	
	def search_materias
		require_post do
			q = Materia.where("CLAVE LIKE ? OR NOMBRE LIKE ?",'%' + params[:query] + '%','%' + params[:query] + '%')
			render :json => q
		end
	end
	
	def show
		@plan = Plane.where(:id => params[:id]).first
	end
	
	def list_materias
		require_post do			
			unless params[:plan_id].nil?
				params[:page] = params[:page].nil? ? 0 : params[:page].to_i
				materias = PlanesMateria.select("planes_materias.id, planes_materias.semestre, materias.clave, materias.nombre").joins("JOIN materias ON materias.id = planes_materias.materia_id").where(:plane_id => params[:plan_id]).order('planes_materias.semestre,materias.clave')
				render :json => {
					:contents => materias					
				}
			end
			
						
		end
	end
	
	private
		def get_carreras
			@carreras = Carrera.order(:siglas).map do |carrera|
				["#{carrera.siglas} - #{carrera.nombre.length > 45 ? (carrera.nombre[0,42] + "...") : carrera.nombre}",carrera.id,]
			end
		end
end
