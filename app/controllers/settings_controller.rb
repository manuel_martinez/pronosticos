class SettingsController < ApplicationController
  before_filter :require_session
  
  def index
    
  end
  
  def change_password
    if @user.change_password(
        params[:pwd_change][:current_password],
        params[:pwd_change][:new_password], 
        params[:pwd_change][:new_password_confirmation]
      )
      flash[:notice] = "Tu contrase&ntilde;a se ha actualizado".html_safe
      redirect_to root_url_for(@user)
    else
      render :action => 'index'
    end
  end
  
  protected
    def root_url_for(user)
      user.is_admin? ? url_for(:controller => :admin, :action => :index) : alumnado_pronosticos_url
    end
end
