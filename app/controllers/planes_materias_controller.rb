class PlanesMateriasController < ApplicationController
  before_filter :require_admin
	def create
		require_post do
			pm = PlanesMateria.new(params[:planes_materia])
			pm.save
			render :json => pm.errors
		end
	end
	
	def delete
		require_post do
			unless params[:id].nil?
				pm = PlanesMateria.where(:id => params[:id]).first
				render :json => pm.destroy	
			end			
		end
	end
end
