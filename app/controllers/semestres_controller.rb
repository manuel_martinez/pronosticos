class SemestresController < ApplicationController
  before_filter :require_admin
  
	SEMESTRES_PER_PAGE = 10
	def index
		
	end
	
	def list
		require_post do
			params[:page] = params[:page].nil? ? 0 : params[:page].to_i
			semestres = Semestre.order("fecha_inicio DESC").limit(SEMESTRES_PER_PAGE).offset(SEMESTRES_PER_PAGE*params[:page])
			render :json => {
				:contents => semestres,
				:total => Semestre.select("COUNT(*) AS num")[0][:num],
				:per_page => SEMESTRES_PER_PAGE
			} 
		end
	end
	
	def new
		
	end
	
	def create
		require_post do
			semestre = Semestre.new(params[:semestre])
			semestre.save
			render :json => semestre.errors
		end		
	end
	
	def edit
		@semestre = Semestre.where(:id => params[:id]).first		
	end
	
	def update
		require_post do
			semestre = Semestre.where(:id => params[:id]).first
			semestre.update_attributes(params[:semestre])
			render :json => semestre.errors
		end
	end
	
	def delete
		require_post do
			unless params[:id].nil?
				semestre = Semestre.where(:id => params[:id]).first
				render :json => semestre.destroy	
			end			
		end
	end
end
