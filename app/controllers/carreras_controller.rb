class CarrerasController < ApplicationController
  before_filter :require_admin  
  
	CARRERAS_PER_PAGE = 10
	def index
		
	end
	
	def list
		require_post do 
			params[:page] = params[:page].nil? ? 0 : params[:page].to_i
			carreras = Carrera.select("carreras.id, carreras.nombre, carreras.siglas, (
									SELECT COUNT(*) FROM alumnos WHERE plane_id IN (
										SELECT id FROM planes WHERE planes.carrera_id = carreras.id
									) 
								) AS num_alumnos").limit(CARRERAS_PER_PAGE).offset(CARRERAS_PER_PAGE*params[:page]).order(:siglas)
			render :json => {
				:contents => carreras,
				:total => Carrera.select("COUNT(*) AS num")[0][:num],
				:per_page => CARRERAS_PER_PAGE
			}								
		end
	end
	
	def new
	
	end
	
	def create	
		require_post do
			carrera = Carrera.new(params[:carrera])
			carrera.save
			render :json => carrera.errors
		end
	end
	
	def edit
		@carrera = Carrera.where(:id => params[:id]).first
	end
	
	def update
		require_post do
			carrera = Carrera.where(:id => params[:id]).first
			carrera.update_attributes(params[:carrera])
			render :json => carrera.errors
		end
	end
end
