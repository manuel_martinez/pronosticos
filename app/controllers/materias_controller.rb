class MateriasController < ApplicationController
  before_filter :require_admin
  
	MATERIAS_PER_PAGE = 10
	OPERATORS = {')' => ')', '(' => '(', '&' => ' AND ', '|' => ' OR '}
	def index
		get_planes
	end
	
	def new
		
	end
	
	def show
		@materia = Materia.where(:id => params[:id]).first
	end
	
	def get_requisitos
	  materia = Materia.select("materias.id, materias.requisitos").where(:id => params[:id]).first
	  s = ""
	  if materia.requisitos
  	  requisitos = materia.requisitos.split
      requisitos.each do |element|
        if element.to_i > 0 
          s += Materia.select("materias.clave").where(:id => element).first[:clave] + " "
        else
          s += OPERATORS[element]
        end
      end  
	  end	  
	  render :json => {:requisitos => s}
	end
	
	def list_materias
		require_post do
			params[:page] = params[:page].nil? ? 0 : params[:page].to_i
			contents = Materia.limit(MATERIAS_PER_PAGE).offset(MATERIAS_PER_PAGE*params[:page]).order(:clave)
			contents = contents.where("materias.id IN (SELECT materia_id FROM planes_materias WHERE plane_id = ?)",params[:plan_id]) if params[:plan_id]
			total = params[:plan_id].nil? ? Materia.select("COUNT(*) as num")[0][:num] : Materia.select("COUNT(*) as num").where("materias.id IN (SELECT materia_id FROM planes_materias WHERE plane_id = ?)",params[:plan_id])[0][:num]
			render :json => {
				:contents => contents,
				:total => total,
				:per_page => MATERIAS_PER_PAGE
			} 
		end		
	end
	
	def create
	  require_post do
	    @materia = Materia.new(params[:materia])
	    @materia.save
	    render :json => @materia.errors
	  end
	end
	
	def edit 
		@materia = Materia.where(:id => params[:id]).first if params[:id]
	end
	
	def update
	  require_post do
	    @materia = Materia.where("id = ?", params[:id])[0]
	    unless @materia.nil?
	      params[:materia][:requisitos] = nil if params[:materia][:requisitos].blank? 
	      @materia.update_attributes(params[:materia])
	      render :json => @materia.errors
	    end
	  end
	end		
	
	def delete
		require_post do
			unless params[:id].nil?
				materia = Materia.where(:id => params[:id]).first				
				render :json => materia.destroy	
			end			
		end
	end
	
	def find
		require_post do	
			q = Materia.where("clave LIKE ? OR nombre LIKE ?",'%' + params[:query] + '%','%' + params[:query] + '%')			
			q = q.where("id NOT IN (?)", params[:selected]) if params[:selected]
			render :json => q
		end
	end	
	
	private
		def get_planes
			@planes = Plane.order(:nombre).map do |plan|
				[plan.nombre, plan.id]
			end
		end
end
