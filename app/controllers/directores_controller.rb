class DirectoresController < ApplicationController
  before_filter :require_not_director
  
  DIRECTORES_PER_PAGE = 15  

  def index
	get_carreras
  end

  def list
	require_post do
	  params[:page] = params[:page].nil? ? 0 : params[:page].to_i
	  directores = Director.select("usuarios.nombre, usuarios.apellidos, directores.id").joins("JOIN usuarios ON usuarios.id = directores.usuario_id").limit(DIRECTORES_PER_PAGE).offset(DIRECTORES_PER_PAGE * params[:page]).order(:apellidos)
	  total = Director.select("COUNT(*) AS num")
	
	  if params[:carrera_id]
		  director_id = Carrera.find(params[:carrera_id]).director_id
		  directores = directores.where(:id => director_id)  
		  total = total.where(:id => director_id)
	  end

	  render :json => {
		:contents => directores,
		:total => total[0][:num],
		:per_page => DIRECTORES_PER_PAGE
	  }				
	end
  end

  def new
		get_carreras
		@usuario = Usuario.new
  end

  def create
  	  require_post do
		  puts params.inspect
		  usuario = Usuario.new(params[:usuario])
		  usuario.is_admin = true
		  if !params[:carrera][:carrera_id].empty? && usuario.save
			  director = Director.new
			  director.usuario_id = usuario.id
			  director.save
			  Carrera.update_director(params[:carrera][:carrera_id], director.id)
			  render :json => director.errors
		  else
			  render :json => usuario.errors
		  end
	  end
  end

  def edit
	  get_carreras
	  @director = Usuario.select("usuarios.nombre, usuarios.apellidos, usuarios.username, directores.id").joins("JOIN directores ON usuarios.id = directores.usuario_id").where("directores.id = ?",params[:id]).first
  end

  def update
	  require_post do
		  director = Director.where(:id => params[:id]).first
		  if !director.nil?
		   	  usuario = Usuario.where(:id => director.usuario_id).first
			  usuario.update_attributes(params[:usuario])
			  unless params[:carrera][:carrera_id].empty?
			  	  Carrera.borrar_director(director.id)
			  	  Carrera.update_director(params[:carrera][:carrera_id], director.id)
			  end
			  render :json => usuario.errors
		  else
		      render :json => director.errors unless director.nil?
		  end
	  end
  end

  def delete
	  require_post do
	      unless params[:id].nil?
		      director = Director.where(:id => params[:id]).first
			  unless director.nil?
				  usuario = Usuario.where(:id => director.usuario_id).first
				  Carrera.borrar_director(director.id)
				  render :json => (usuario.delete && director.delete)				
			  end
		  end			
	  end
  end

  private
    def get_carreras
      @carreras = Carrera.order(:siglas).map do |carrera|
	    [carrera.siglas, carrera.id]
      end
    end
end
