class ApplicationController < ActionController::Base
  # protect_from_forgery
  
  protected
  
  def require_post
    if request.post?
      yield
    else
      redirect_to :root
    end
  end
  
  def require_session
    @user = Usuario.find session[:user_id] if session[:user_id]
    if @user.nil?
      flash[:notice] = "Por favor inicia sesion" 
      redirect_to login_url 
    end
  end

  def require_not_director
		@user = Usuario.find session[:user_id] if session[:user_id]
    unless @user && @user.is_admin? && !@user.is_director?
      flash[:notice] = "Por favor inicia sesion" 
      redirect_to login_url 
    end
  end
  
  def require_admin
    @user = Usuario.find session[:user_id] if session[:user_id]
    unless @user && @user.is_admin?
      flash[:notice] = "Por favor inicia sesion" 
      redirect_to login_url 
    end
  end
  
  # Regresa el siguiente semestre
  def next_semester
    semestre = Semestre.where("fecha_inicio > ?", Time.now).order('fecha_inicio').first
		semestre ||= Semestre.order('fecha_inicio').last
  end
end
