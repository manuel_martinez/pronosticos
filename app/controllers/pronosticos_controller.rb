class PronosticosController < ApplicationController
  before_filter :require_admin
	before_filter :set_usuario, :only => [:list_pronosticos, :list_alumnos, :pending_students, :incomplete_students, :list_programas_internacionales, :list_candidatos]

  MAX_MATERIAS = 7 #Cuantas materias puede pronosticar un alumno para un semestre
  PRONOSTICOS_PER_PAGE = 30
  
  def index
    get_plans
    get_semesters
  end  
  
  def search
    
  end 
  
  def list_pronosticos
    require_post do 
      set_page 		
		  set_semestre_id
      contents = Pronostico.total_count.where(:semestre_id => params[:semestre_id]).limit(PRONOSTICOS_PER_PAGE).offset(PRONOSTICOS_PER_PAGE * params[:page])                        
      total = Pronostico.select("COUNT(*) AS num, pronosticos.materia_id").where(:semestre_id => params[:semestre_id]).group("pronosticos.materia_id")
      
      if params[:plan_id] 
        contents = contents.by_plan_id params[:plan_id]
        total = total.by_plan_id params[:plan_id]
      end

			if @usuario.is_director?
				contents = contents.by_plan_id Plane.por_director(@usuario.id)
        total = total.by_plan_id Plane.por_director(@usuario.id)
			end
      
      render :json => {
        :contents => contents,
        :total    => total.length,
        :per_page => PRONOSTICOS_PER_PAGE
      }
    end
  end

	def list_candidatos
    require_post do 
      set_page 		
		  set_semestre_id
			contents = Alumno.candidate_for params[:semestre_id]
      
      if params[:plan_id] 
        contents = contents.where("plane_id = (?)", params[:plan_id])
      end

			if @usuario.is_director?
				contents = contents.where("plane_id IN (?)", Plane.por_director(@usuario.id))
			end

      render :json => {
        :contents => contents,
        :total    => contents.length,
        :per_page => PRONOSTICOS_PER_PAGE
      }
    end
  end

	def list_programas_internacionales
    require_post do 
      set_page 		
		  set_semestre_id
			contents = Alumno.with_international_program_for params[:semestre_id]
      
      if params[:plan_id] 
        contents = contents.where("plane_id = (?)", params[:plan_id])
      end

			if @usuario.is_director?
				contents = contents.where("plane_id IN (?)", Plane.por_director(@usuario.id))
			end

      render :json => {
        :contents => contents,
        :total    => contents.length,
        :per_page => PRONOSTICOS_PER_PAGE
      }
    end
  end
  
  def list_alumnos
    require_post do 
      set_page
      set_semestre_id
      contents = Pronostico.with_alumno.where(:materia_id => params[:materia_id], :semestre_id => params[:semestre_id]).limit(PRONOSTICOS_PER_PAGE).offset(PRONOSTICOS_PER_PAGE*params[:page])
			total = Pronostico.select("COUNT(*) AS num").where(:materia_id => params[:materia_id], :semestre_id => params[:semestre_id])

			if @usuario.is_director?
				contents = contents.by_plan_id Plane.por_director(@usuario.id)
        total = total.by_plan_id Plane.por_director(@usuario.id)
			end
			
      render :json => {
        :contents => contents,
        :total    => total[0][:num],
        :per_page => PRONOSTICOS_PER_PAGE
      }
    end
  end
  
  def download_csv
    set_semestre_id
    path = Pronostico.make_csv_file(params[:semestre_id])
    send_file path, :type => "application/csv"
  end
  
  def pending_students
    @semestre = next_semester
    @pending_students = Alumno.without_predictions_for(@semestre.id).filtro_director(@usuario)
  end
  
  def incomplete_students
    @semestre = next_semester
    @incomplete_students = Alumno.with_incomplete_predictions_for(@semestre.id).filtro_director(@usuario)
  end

  def international_programs
		get_plans
		get_semesters
		@semestre = next_semester
  end
  
	def candidates
    get_plans
		get_semesters
		@semestre = next_semester
  end

  def show 
    @materia = Materia.find(params[:id])
    get_semesters
  end
  
  private
    def get_plans
			usuario = Usuario.find session[:user_id]
			if usuario.is_director?
				@planes = Plane.por_director(usuario.id).map do |id|
					plan = Plane.find(id)
					[plan.nombre, plan.id]
				end
			else			
				@planes = Plane.order(:nombre).map do |plan|
					[plan.nombre, plan.id]
				end
			end
    end

    def set_usuario
			@usuario = Usuario.find session[:user_id] 
		end
    
    def set_page
      params[:page] = params[:page].nil? ? 0 : params[:page].to_i
    end
    
    def set_semestre_id
      params[:semestre_id] = params[:semestre_id].nil? ? next_semester.id : params[:semestre_id]
    end
    
    def get_semesters
      @semestres = Semestre.order("fecha_inicio DESC")
    end
end
