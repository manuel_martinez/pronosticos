class Usuario < ActiveRecord::Base
	validates_presence_of [:nombre,:apellidos, :username]
	validates_uniqueness_of :username
	validate :password_non_blank
	
	def alumno
    @alumno ||= Alumno.select([:id, :plane_id]).where(:usuario_id => self.id).first
  end

	def director
		@director ||= Director.select([:id]).where(:usuario_id => self.id).first 
	end

	def is_director?
		@is_director ||= !self.director.nil?
	end
	
	def is_admin?
    self.is_admin
  end
  
  def full_name 
    "#{self.nombre} #{self.apellidos}"
  end
	
	def password
		return @password
	end
	
	def password=(pwd)
		@password=pwd
		return if @password.blank?		
		self.encrypted_password = Usuario.encrypt_password(self.password)
	end
	
	def change_password(current_password, new_password, new_password_confirmation)
	  if self.encrypted_password.eql? Usuario.encrypt_password(current_password)
  	  unless new_password.eql? new_password_confirmation  
    	  self.errors.add(:password, "Las contrase&ntilde;as no coinciden") 
    	  return false
  	  end	    
	    self.password = new_password
	    return self.save
    else
      self.errors.add(:password, "La contrase&ntilde;a actual no es correcta")
      return false
	  end
	end
	
	def self.authenticate(username, password)
		usuario = self.where(:username => username).first
		if usuario
			given_passwd = Usuario.encrypt_password(password)
			if usuario.encrypted_password != given_passwd
				usuario = nil
			end
		end
		return usuario
	end
	
	private
	
		def self.encrypt_password(password)
			string = password + "1t3sM"
			Digest::SHA1.hexdigest(string)
		end
		
		def password_non_blank
			errors.add(:password, "Missing password") if encrypted_password.blank?
		end
end
