class PlanesMateria < ActiveRecord::Base
	set_table_name :planes_materias
	validates_presence_of [:semestre, :materia_id, :plane_id]
	validates_uniqueness_of :materia_id, :scope => :plane_id
	validates_numericality_of :semestre
end
