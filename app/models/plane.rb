class Plane < ActiveRecord::Base
	validates_presence_of [:nombre, :carrera_id]

	def self.por_director(director_id)
		@planes_director = Plane.select(:id).where("carrera_id IN (?)", (Carrera.select("carreras.id").joins("JOIN directores ON directores.id = carreras.director_id").where("directores.usuario_id = ?", director_id)))
	end
	
	# Regresa la carrera a la que pertenece el plan
	def carrera
	  @carrera ||= Carrera.select([:id, :nombre, :siglas]).where(:id => self.carrera_id).first
  end
end
