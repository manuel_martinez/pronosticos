class MateriasCursada < ActiveRecord::Base			
	set_table_name :materias_cursadas
	belongs_to :materia
	validates_presence_of [:status, :materia_id]
	validates_uniqueness_of :materia_id, :scope => :alumno_id, :message => 'Ya se encuentra en la historia academica del alumno'
	
	STATUS = [
				['Cursando',0],
				['Aprobada',1],
				['Programa internacional', 2]
			]

end
