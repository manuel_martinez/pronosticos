class Materia < ActiveRecord::Base
  set_table_name :materias
	has_many :materias_cursadas
	has_many :pronosticos
	has_many :planes_materias
	has_many :planes, :through => :planes_materias
	validates_presence_of [:clave, :nombre]
	validates_uniqueness_of :clave
	
	##
	# Regresa el semestre en el que esta materia se encuentra para
	# el plan de estudios dado por plane_id
	def semestre_for(plane_id=nil)
	  @semestre_for ||= PlanesMateria.select(:semestre).where(:materia_id => self.id, :plane_id => plane_id).first.semestre
	end	
end
