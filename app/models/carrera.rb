class Carrera < ActiveRecord::Base
	has_many :planes
	belongs_to :director
	validates_presence_of [:siglas,:nombre]

	def self.borrar_director(id)
		Carrera.where(:director_id => id).each do |carrera|
			carrera.director_id = nil
			carrera.save
		end
	end

	def self.update_director(carrera_ids, director)
		carrera_ids.each do |id|
			carrera = Carrera.find(id)
			carrera.director_id = director
	   	    carrera.save
		end
	end
end
