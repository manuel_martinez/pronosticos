class Pronostico < ActiveRecord::Base
  scope :total_count, select("COUNT(*) AS num, pronosticos.materia_id, materias.clave, materias.nombre").joins("JOIN materias ON materias.id = pronosticos.materia_id").group("pronosticos.materia_id, materias.clave, materias.nombre").order("num DESC")  
                 
  scope :by_plan_id, lambda { |plan_id| where("pronosticos.alumno_id IN (SELECT id FROM alumnos WHERE alumnos.plane_id IN (?))", plan_id) }
  scope :with_alumno, select("usuarios.nombre, usuarios.apellidos, usuarios.username, alumnos.id AS alumno_id").joins("JOIN alumnos ON alumnos.id = pronosticos.alumno_id JOIN usuarios ON usuarios.id = alumnos.usuario_id")
  
  validates_uniqueness_of :alumno_id, :scope => [:materia_id, :semestre_id]  

  MinNumberPerSemester = 6 # Número de materias a pronosticar para considerar que el pronostico fue completo  

  def self.stats_for_semestre(semestre_id, usuario)
    stats = {}
    alumnos = Pronostico.alumnos_for_semester(semestre_id) 
		unless alumnos.empty?    
		  alumnos.filtro_director(usuario).each do |alumno|
        stats[alumno.carrera.siglas] = stats[alumno.carrera.siglas].nil? ? 1 : stats[alumno.carrera.siglas] + 1
      end
    end
    stats
  end
  
  # Regresa los alumnos que realizaron sus pronosticos en 
  # el semestre con id +semestre_id+
  def self.alumnos_for_semester(semestre_id)
    alumno_ids = Pronostico.select(:alumno_id).where(:semestre_id => semestre_id).group(:alumno_id).map { |pronostico| pronostico.alumno_id }
		alumno_ids += ProgramaInternacional.select(:alumno_id).where(:semestre_id => semestre_id).map { |programa| programa.alumno_id }
    if alumno_ids.any?
      @alumnos = Alumno.where("id IN (?)", alumno_ids)
    else
      @alumnos = []
    end
    @alumnos
  end
  
  def self.tempfile_path
    File.join Rails.root, 'tmp'
  end
  
  def self.make_csv_file(semestre_id)
    file = File.open( File.join(Alumno.tempfile_path, "pronosticos_#{Time.now.to_i}.csv"), 'wb')
    file.write "Clave, Nombre Materia, Pronostico\n"
    Pronostico.total_count.where(:semestre_id => semestre_id).each do |pronostico|
      file.write "#{pronostico.clave}, #{pronostico.nombre}, #{pronostico.num}\n"
    end      
    file.close
    file.path    
  end
  


end 
