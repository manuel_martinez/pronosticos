class Director < ActiveRecord::Base
	set_table_name "directores"
	belongs_to :usuario
	has_many :carreras

	def carrera
		@carrera ||= Carrera.select([:id]).where(:director_id => self.id)
	end

end
