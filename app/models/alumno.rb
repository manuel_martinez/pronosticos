class Alumno < ActiveRecord::Base
	belongs_to :usuario
	belongs_to :plane
	has_many :pronosticos
	has_many :materias_cursadas
	validates_presence_of :plane_id

	# Si el usuario es director, solo tiene acceso a los alumnos de su plan
	def self.filtro_director(current_user)
	 	if current_user.is_director?
    	where("alumnos.plane_id IN (?)", Plane.por_director(current_user.id))
		else
			scoped
		end
	end

  def self.without_predictions_for(semestre_id)
    alumno_ids = Pronostico.select(:alumno_id).where(:semestre_id => semestre_id).group(:alumno_id).map { |pronostico| pronostico.alumno_id }
		alumno_ids += ProgramaInternacional.select(:alumno_id).where(:semestre_id => semestre_id).map { |programa| programa.alumno_id }
    if alumno_ids.any?
      @alumnos = Alumno.where("id NOT IN (?)", alumno_ids)
    else
      @alumnos = Alumno.scoped
    end
    @alumnos
  end

  def self.with_incomplete_predictions_for(semestre_id)
    alumno_ids = []
    Pronostico.where(:semestre_id => semestre_id).group(:alumno_id).count(:alumno_id).each_pair do |alumno_id, numero_pronosticos|
      alumno_ids << alumno_id if numero_pronosticos < Pronostico::MinNumberPerSemester
    end
    if alumno_ids.any?
      @alumnos = Alumno.where("id IN (?)", alumno_ids)
    else
      @alumnos = Alumno.where('1 = 0')
    end
    @alumnos
  end

	def self.with_international_program_for(semestre_id)
		alumno_ids = ProgramaInternacional.select(:alumno_id).where(:semestre_id => semestre_id).map { |programa| programa.alumno_id }
		if alumno_ids.any?
			@alumnos = Alumno.select("alumnos.id, alumnos.plane_id, alumnos.usuario_id, usuarios.username, usuarios.nombre, usuarios.apellidos").joins("JOIN usuarios ON usuarios.id = alumnos.usuario_id").where("alumnos.id IN (?)", alumno_ids)
		else
			@alumnos = Alumno.where('1 = 0')
		end
		@alumnos
	end

  def self.candidate_for(semestre_id)
		alumno_ids = Candidato.select(:alumno_id).where(:semestre_id => semestre_id).map { |candidato| candidato.alumno_id }
		if alumno_ids.any?
			@alumnos = Alumno.select("alumnos.id, alumnos.plane_id, alumnos.usuario_id, usuarios.username, usuarios.nombre, usuarios.apellidos").joins("JOIN usuarios ON usuarios.id = alumnos.usuario_id").where("alumnos.id IN (?)", alumno_ids)
		else
			@alumnos = Alumno.where('1 = 0')
		end
		@alumnos
  end

  def number_of_predictions_for(semestre_id)
    Pronostico.where(:alumno_id => self.id, :semestre_id => semestre_id).count(:id)
  end

  # Regresa un arreglo de materias.
  def predictions_by_semestre_id(semestre_id)
    Materia.select("materias.id, materias.clave, materias.status, materias.nombre").joins("JOIN pronosticos ON pronosticos.materia_id = materias.id").where("pronosticos.alumno_id = ? AND pronosticos.semestre_id = ?", self.id, semestre_id)
  end

	# Regresa verdadero si el alumno pronosticó PI
	def programa_internacional_by_semestre_id(semestre_id)
		ProgramaInternacional.where("alumno_id = ? AND semestre_id = ?", self.id, semestre_id).any?
	end

	# Regresa verdadero si el alumno es candidato a graduar
	def candidato_by_semestre_id(semestre_id)
		Candidato.where("alumno_id = ? AND semestre_id = ?", self.id, semestre_id).any?
	end

  # Regresa el plan del alumno
  def plan
    @plan ||= Plane.select([:id, :nombre, :carrera_id]).where(:id => self.plane_id).first
  end

  # Regresa la carrera del alumno
  def carrera
    @carrera ||= self.plan.carrera
  end

  # Regresa el usuario ligado con este alumno
  def usuario
    @usuario ||= Usuario.select([:id, :nombre, :apellidos, :username]).where(:id => self.usuario_id).first
  end

	# Regresa verdadero en caso de que el alumno ya haya registrado pronósticos
	# para el semestre dado
	def has_predictions_for?(semester)
	   (Pronostico.where(:semestre_id => semester.id, :alumno_id => self.id).count(:id) > 0) || (self.programa_internacional_by_semestre_id(semester.id))
  end

  def reset_predictions_for(semester_id)
    Pronostico.delete_all(:semestre_id => semester_id, :alumno_id => self.id)
		ProgramaInternacional.delete_all(:semestre_id => semester_id, :alumno_id => self.id)
		Candidato.delete_all(:semestre_id => semester_id, :alumno_id => self.id)
  end

	# Regresa las materias que el alumno podría
	# pronosticar dependiendo de su historial académico y
	# la regla de los tres semestres
	def possible_subjects
	  materias_cursadas = MateriasCursada.select(:materia_id).where(:alumno_id => self.id).map { |m| m.materia_id }

	  query = PlanesMateria.select("planes_materias.semestre").where("plane_id = ?", self.plane_id).order("planes_materias.semestre")
    query = query.where("planes_materias.materia_id NOT IN (?)", materias_cursadas) unless materias_cursadas.empty?
    semestre = query.limit(1).first.semestre # Semestre mas atrasado

    # Regla de los 3 semestres
    possible_subjects = Materia.select("materias.id, materias.nombre, materias.clave, materias.requisitos, planes_materias.semestre").where("planes_materias.semestre IN (?) AND planes_materias.plane_id = ?", (semestre..semestre+self.semestres_regla).to_a, self.plane_id).joins("JOIN planes_materias ON planes_materias.materia_id = materias.id").order("planes_materias.semestre")

    # Eliminamos materias ya cursadas
    possible_subjects = possible_subjects.where("materias.id NOT IN (?)", materias_cursadas) unless materias_cursadas.empty?

    # Validamos requisitos
    possible_subjects.map do |materia|
      materia if self.can_take? materia
    end.compact
	end


	# Regresa verdadero si el alumno cumple con los
	# requisitos de la materia
	def can_take?(materia)
    materia.requisitos.nil? || eval_requirements(materia.requisitos.split " ")
	end

  private

	  # Evalúa un arreglo conteniendo los requisitos de la materia
	  # el arreglo debe contener sólo números
	  #  y '(' ')' '&' '|'
    def eval_requirements(arr)
  	  left = nil
      length = arr.length
      i = 0
      while i < length
        case arr[i]
        when '('
          match_index = matching_parentheses_index(arr)
          left = eval_requirements(arr[i+1, match_index - 1])
          i = match_index
        when '&'
          left &&= eval_requirements(arr[i+1,length])
          break
        when '|'
          left ||= eval_requirements(arr[i+1,length])
          break
        else
          left = !MateriasCursada.select(:id).where(:alumno_id => self.id, :materia_id => arr[i].to_i).empty?
        end
        i += 1
      end
      left
    end

    def self.tempfile_path
      File.join Rails.root, 'tmp'
    end

    def self.make_csv_file(alumnos)
      file = File.open( File.join(Alumno.tempfile_path, "alumnos_#{Time.now.to_i}.csv"), 'wb')
      alumnos.each do |alumno|
        file.write "#{alumno.usuario.username}, #{alumno.usuario.nombre}, #{alumno.usuario.apellidos}, #{alumno.carrera.siglas}\n"
      end
      file.close
      file.path
    end


  	# Regresa el índice del parentesís que cierra
  	def matching_parentheses_index(arr)
      i = 1
      open_parentheses = 1
      while open_parentheses > 0
        open_parentheses += 1 if arr[i] == '('
        open_parentheses -= 1 if arr[i] == ')'
        i += 1
      end
      i - 1
  	end

end
