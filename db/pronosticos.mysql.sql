
CREATE TABLE usuarios (
                id INT AUTO_INCREMENT NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                apellidos VARCHAR(50) NOT NULL,
                username VARCHAR(50) NOT NULL,
                encrypted_password VARCHAR(50) NOT NULL,
                is_admin BOOLEAN DEFAULT false NOT NULL,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE semestres (
                id INT AUTO_INCREMENT NOT NULL,
                fecha_inicio DATE NOT NULL,
                fecha_fin DATE NOT NULL,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE materias (
                id INT AUTO_INCREMENT NOT NULL,
                clave VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                status INT DEFAULT 0 NOT NULL,
                requisitos TEXT,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE carreras (
                id INT AUTO_INCREMENT NOT NULL,
                nombre VARCHAR(100) NOT NULL,
                siglas VARCHAR(3) NOT NULL,
								director_id INT,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE planes (
                id INT AUTO_INCREMENT NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                carrera_id INT NOT NULL,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE planes_materias (
                id INT AUTO_INCREMENT NOT NULL,
                semestre INT NOT NULL,
                plane_id INT NOT NULL,
                materia_id INT NOT NULL,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE alumnos (
                id INT AUTO_INCREMENT NOT NULL,
                plane_id INT NOT NULL,
                usuario_id INT NOT NULL,
								semestres_regla INT DEFAULT 2 NOT NULL,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE directores (
								id INT AUTO_INCREMENT NOT NULL,
								usuario_id INT NOT NULL,
								created_at DATETIME NOT NULL,
								updated_at DATETIME NOT NULL,
								PRIMARY KEY (id)
);


CREATE TABLE pronosticos (
                id INT AUTO_INCREMENT NOT NULL,
                alumno_id INT NOT NULL,
                materia_id INT NOT NULL,
                semestre_id INT NOT NULL,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE candidatos (
                id INT AUTO_INCREMENT NOT NULL,
                alumno_id INT NOT NULL,
                semestre_id INT NOT NULL,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE programas_internacionales (
                id INT AUTO_INCREMENT NOT NULL,
                alumno_id INT NOT NULL,
                semestre_id INT NOT NULL,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE materias_cursadas (
                id INT AUTO_INCREMENT NOT NULL,
                status INT DEFAULT 0 NOT NULL,
                alumno_id INT NOT NULL,
                materia_id INT NOT NULL,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


ALTER TABLE alumnos ADD CONSTRAINT usuarios_alumnos_fk
FOREIGN KEY (usuario_id)
REFERENCES usuarios (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE directores ADD CONSTRAINT usuarios_directores_fk
FOREIGN KEY (usuario_id)
REFERENCES usuarios (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE pronosticos ADD CONSTRAINT semestres_pronosticos_fk
FOREIGN KEY (semestre_id)
REFERENCES semestres (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE programas_internacionales ADD CONSTRAINT semestres_programas_internacionales_fk
FOREIGN KEY (semestre_id)
REFERENCES semestres (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE candidatos ADD CONSTRAINT semestres_candidatos_fk
FOREIGN KEY (semestre_id)
REFERENCES semestres (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE materias_cursadas ADD CONSTRAINT materias_alumnos_materias_fk
FOREIGN KEY (materia_id)
REFERENCES materias (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE planes_materias ADD CONSTRAINT materias_planes_materias_fk
FOREIGN KEY (materia_id)
REFERENCES materias (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE pronosticos ADD CONSTRAINT materias_pronosticos_fk
FOREIGN KEY (materia_id)
REFERENCES materias (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE planes ADD CONSTRAINT carreras_planes_fk
FOREIGN KEY (carrera_id)
REFERENCES carreras (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE alumnos ADD CONSTRAINT planes_alumnos_fk
FOREIGN KEY (plane_id)
REFERENCES planes (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE carreras ADD CONSTRAINT directores_carreras_fk
FOREIGN KEY (director_id)
REFERENCES directores (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE planes_materias ADD CONSTRAINT planes_planes_materias_fk
FOREIGN KEY (plane_id)
REFERENCES planes (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE materias_cursadas ADD CONSTRAINT alumnos_alumnos_materias_fk
FOREIGN KEY (alumno_id)
REFERENCES alumnos (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE pronosticos ADD CONSTRAINT alumnos_pronosticos_fk
FOREIGN KEY (alumno_id)
REFERENCES alumnos (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE programas_internacionales ADD CONSTRAINT alumnos_programas_internacionales_fk
FOREIGN KEY (alumno_id)
REFERENCES alumnos (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE candidatos ADD CONSTRAINT alumnos_candidatos_fk
FOREIGN KEY (alumno_id)
REFERENCES alumnos (id)
ON DELETE CASCADE
ON UPDATE CASCADE;
