# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 0) do

  create_table "alumnos", :force => true do |t|
    t.integer  "plane_id",   :null => false
    t.integer  "usuario_id", :null => false
    t.integer  "semestres_regla", :default => 2, :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "alumnos", ["plane_id"], :name => "planes_alumnos_fk"
  add_index "alumnos", ["usuario_id"], :name => "usuarios_alumnos_fk"

  create_table "carreras", :force => true do |t|
    t.string   "nombre",      :limit => 100, :null => false
    t.string   "siglas",      :limit => 3,   :null => false
    t.integer  "director_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "carreras", ["director_id"], :name => "directores_carreras_fk"

  create_table "directores", :force => true do |t|
    t.integer  "usuario_id", :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "directores", ["usuario_id"], :name => "usuarios_directores_fk"

  create_table "materias", :force => true do |t|
    t.string   "clave",      :limit => 50,                :null => false
    t.string   "nombre",     :limit => 50,                :null => false
    t.integer  "status",                   :default => 0, :null => false
    t.text     "requisitos"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
  end

  create_table "materias_cursadas", :force => true do |t|
    t.integer  "status",     :default => 0, :null => false
    t.integer  "alumno_id",                 :null => false
    t.integer  "materia_id",                :null => false
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "materias_cursadas", ["alumno_id"], :name => "alumnos_alumnos_materias_fk"
  add_index "materias_cursadas", ["materia_id"], :name => "materias_alumnos_materias_fk"

  create_table "planes", :force => true do |t|
    t.string   "nombre",     :limit => 50, :null => false
    t.integer  "carrera_id",               :null => false
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "planes", ["carrera_id"], :name => "carreras_planes_fk"

  create_table "planes_materias", :force => true do |t|
    t.integer  "semestre",   :null => false
    t.integer  "plane_id",   :null => false
    t.integer  "materia_id", :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "planes_materias", ["materia_id"], :name => "materias_planes_materias_fk"
  add_index "planes_materias", ["plane_id"], :name => "planes_planes_materias_fk"

  create_table "pronosticos", :force => true do |t|
    t.integer  "alumno_id",   :null => false
    t.integer  "materia_id",  :null => false
    t.integer  "semestre_id", :null => false
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "pronosticos", ["alumno_id"], :name => "alumnos_pronosticos_fk"
  add_index "pronosticos", ["materia_id"], :name => "materias_pronosticos_fk"
  add_index "pronosticos", ["semestre_id"], :name => "semestres_pronosticos_fk"

  create_table "semestres", :force => true do |t|
    t.date     "fecha_inicio", :null => false
    t.date     "fecha_fin",    :null => false
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "usuarios", :force => true do |t|
    t.string   "nombre",             :limit => 50,                    :null => false
    t.string   "apellidos",          :limit => 50,                    :null => false
    t.string   "username",           :limit => 50,                    :null => false
    t.string   "encrypted_password", :limit => 50,                    :null => false
    t.boolean  "is_admin",                         :default => false, :null => false
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
  end

end
