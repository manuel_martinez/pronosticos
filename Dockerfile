FROM ubuntu
EXPOSE 3000
SHELL ["/bin/bash", "-c"]
# Update packages
RUN apt-get update && apt-get dist-upgrade -y
# Install rvm
RUN apt-get install software-properties-common -y
RUN apt-add-repository -y ppa:rael-gc/rvm
RUN apt-get update
RUN apt-get install rvm -y
SHELL [ "/bin/bash", "-l", "-c" ]
# Install ruby 1.9.3
RUN rvm install 2.0.0
# Copy project inside container (files ignored by .dockerignore)
COPY / /home/pronosticos/
# Mysql client
RUN apt-get install libmysqlclient-dev -y
# Install pronosticos gems
RUN cd /home/pronosticos/ && \
gem install bundler -v '~> 1.0.0' && \
gem install mysql2 -v 0.2.24 && \
bundle install
# Start server
# Change shell mode
# SHELL ["/bin/bash", "-c"]
RUN chmod 777 /home/pronosticos/start_server.sh
CMD /home/pronosticos/start_server.sh
