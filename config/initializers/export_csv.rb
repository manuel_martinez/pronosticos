class ActiveRecord::Base
  def self.export_csv 
    f = File.new(File.expand_path("db/#{table_name}.csv",Rails.root),"w")
    data = ""
    self.find(:all).each do |record|         
      column_names.each do |key|
        data += "#{record.attributes[key]}," 
      end
      data = data.chop
      data += "\r\n"
      f.write data            
    end
    print data
    
  end  
      
end