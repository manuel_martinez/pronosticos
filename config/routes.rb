Pronosticos::Application.routes.draw do

  namespace :alumnado do
    match 'pronosticos/' => 'pronosticos#index', :via => :get
    match 'pronosticos/create' => 'pronosticos#create', :via => :post
    match 'pronosticos/thank_you' => 'pronosticos#thank_you'
    match 'pronosticos/changetoadmin' => 'pronosticos#changetoadmin'
  end

  match '/settings' => 'settings#index', :via  => :get, :as => :settings
  match 'main/login' => 'main#login', :via => :get, :as => :login
  match 'admin/logout' => 'admin#logout', :via => :get, :as => :logout

	root :to => "main#login"
  match ':controller(/:action(/:id(.:format)))'

end
