/**
 * @author Manuel Martínez
 */
(function(){
	$.jumper = function(options) {
		var nil = $('');
		var _self = {
			url: '',
			nav: null,
			nav_original_position: {},
			content_header: nil,
			params: {},
			contents_container: null,
			content_header_current_height: 0,
			double_content_header_height: 0,
			content_header_height: 0,
			onResponse: function(){},
			init: function() {
				_self.nav_original_position = _self.nav.position();
				_self.content_header_height = _self.content_header.outerHeight();
				_self.double_content_header_height = 2*_self.content_header_height;
				_self.content_header_current_height = _self.double_content_header_height;
				_self.setScroll();
				_self.doPost();
			},
			doPost: function() {
				_self.nav.hide().html('');
				$.post(_self.url,_self.params, function(data){
					_self.onResponse(data);
					_self.setAnchors();
					_self.showNav();
				});
			},
			showNav: function() {
				_self.nav.hide().fadeIn(500);
			},
			addToNav: function(options) {
				var link = $('<a href="#" class="anchor_link" rel="' + options.rel + '">' + options.name + '</a>');				
				var li = $('<li></li>');
				link.appendTo(li);
				li.appendTo(_self.nav);
			},
			setAnchors: function() {
				_self.nav.find('.anchor_link').click(function(e){
					e.preventDefault();
					var id = $(this).attr('rel');								
					var new_top = _self.contents_container.find('#'+ id).position().top - _self.content_header_current_height 				
					$('body, html').animate({
						scrollTop: new_top
					}, 500);
				});
			},
			setScroll: function() {
				$(window).scroll(function(){				
					if($(this).scrollTop() > _self.nav_original_position.top) {
						_self.content_header_current_height = _self.content_header_height;
						_self.content_header.css({
							'position': 'fixed',
							'top': 0,						
							'box-shadow': '0 2px 2px -1px rgba(0,0,0,.1)'						 
						});
						_self.nav.css({
							'position': 'fixed',
							'top': 10,
							'width': 195						
						});
					}
					else {
						_self.content_header_current_height = _self.double_content_header_height;
						_self.content_header.css({
							'position': 'static',
							'box-shadow': 'none'						
						});	
						_self.nav.css({
							'position': 'static'						
						});
					}
				});
			}
			
		};
		$.extend(_self, options);
		_self.init();
		return _self
	};	
})(jQuery);
