/**;
 * @author Manuel Martínez
 */
(function(){
	$.dropdown = function(options) {
		var _self = {
			//Obligatorios
			trigger: null,
			content: null,
			//Opcionales
			visible: false,			
			wrapper: null,			
			triggerEvent: 'click',
			dropdown_container: null,
			mouse_inside: false,
			css: {
				'position': 'absolute',				
				'left': 0,
				'height': '2px',
				'min-width': '150px',
				'opacity': 0,
				'background-color': '#FFF',
				'display': 'none',
				'border': '1px solid #CCC',
				'box-shadow': '0 0 2px #EAEAEA',
				'-moz-box-shadow': '0 0 2px #EAEAEA',
				'-webkit-box-shadow': '0 0 2px #EAEAEA',
				'z-index': 3000
			},
			wrapper_css: {},
			init: function(){				
				_self.initContainer();
				_self.initWrapper();
				_self.initContent();
				_self.setTrigger();
				_self.setContainer();				
				_self.trigger.appendTo(_self.wrapper);
				_self.dropdown_container.appendTo(_self.wrapper);
				$('body').click(function(){
					if(!_self.mouse_inside) {
						_self.hideContainer();
					}
				});
			},			
			initContent: function() {
				_self.content.appendTo(_self.dropdown_container);
			},
			initWrapper: function() {
				var s = '<div class="dropdown_wrapper"></div>';				
				_self.wrapper = $(s).css($.extend( {'position': 'relative'}, _self.wrapper_css ));
				_self.wrapper.appendTo(_self.trigger.parent());								
			},
			initContainer: function() {
				var s = '<div class="dropdown_container"></div>';				
				_self.dropdown_container = $(s).css(_self.css);				
				_self.dropdown_container.css('top',_self.trigger.outerHeight() - 1);				
			},
			setTrigger: function() {
				_self.trigger.bind(_self.triggerEvent, function(e){
					e.preventDefault();					
					if(!_self.visible){						
						_self.showContainer();							
					}	
					else {
						_self.hideContainer();
					}
				});					
				_self.trigger.hover(function(){
					_self.mouse_inside = true;
				}, function(){
					_self.mouse_inside = false;
				});
			},
			setContainer: function() {
				_self.dropdown_container.hover(function(){
					_self.mouse_inside = true;
				}, function(){
					_self.mouse_inside = false;
				});				
			},
			showContainer: function() {
				_self.dropdown_container.show().animate({
					'height': _self.content.outerHeight(),
					'opacity': 1
				}, 250);
				_self.visible = true;				
			},
			hideContainer: function() {
				_self.dropdown_container.animate({
					'height': 2,
					'opacity': 0
				}, 250, function(){
					_self.dropdown_container.hide()
				})
				_self.visible = false;
			}
		};		
		$.extend(_self,options);
		_self.init();
		return _self;
	};
})(jQuery);
