(function(){
	$.form = function(options){
		var _nil = $('');
		var _self = {
			container: _nil,
			error_container: null,
			loader_container: null,
			init: function(){
				_self.initErrorContainer();
				_self.container.submit(_self.onSubmit);
			},
			initErrorContainer: function(){
				var s = '';
				s += '<div class="form_error_notice">';
				s += '</div>';				
				_self.error_container = $(s).hide();
				_self.error_container.prependTo(_self.container);				
			},
			onSubmit: function(e){
				e.preventDefault();
				_self.container.find('input[type="submit"]').attr('disabled',true).addClass('disabled_btn');
				$.post(_self.container.attr('action'), _self.container.serialize(),  _self.onResponse);
			},
			onResponse: function(data){
				var s = '';
				var length = 0;
				_self.container.find('input[type="submit"]').attr('disabled',false).removeClass('disabled_btn');
				$.each(data, function(field, errors){
					++length;
					s += _self.parseError(field, errors);
				});
				if(length == 0){
					_self.onSuccess();
				}
				else{
					_self.error_container.html(s).hide().fadeIn(500);	
				}
			},
			parseError: function(field, errors){
				var s = '<div class="form_field_error">';
				s += '<b>' + field + ':</b> ';
				$.each(errors, function(key, error){
					s += error + ', ';
				});
				s = s.substring(0, s.length - 2);
				s += '</div>'
				return s;
			},
			onSuccess: function(){
				alert("It worked!");
			}
		};
		$.extend(_self, options);
		_self.init();
		return _self;
	};
})(jQuery);
