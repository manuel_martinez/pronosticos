/**
 * @author Manuel Martínez
 */
var _zindex = 8000;
(function(){
	$.dialog = function(options) {
		var nil = $('');		
		var _self = {
			params: null,
			url: null,
			ajax: false,
			width: 400,
			min_height: 200,
			autoshow: true,
			overlay: null,
			body: null,
			color: "#000",
			opacity: 0.8,
			title: '',
			msg: '',
			msg_container: nil,
			init: function(){
				_self.body = $('body');				
				_self.initOverlay();				
				if(_self.ajax) {
					_self.doPost();
				}
				else {
					_self.renderMsg(_self.msg);
				}
				$(window).resize(function(){
					_self.overlay.css({
						width: _self.body.width(),
						height: _self.body.height()						
					});					
				});
			},
			initOverlay: function(){
				var s = '';
				s += '<div class="dialog_overlay"></div>';
				_self.overlay = $(s).css({
					"z-index": ++_zindex,
					display: "none",
					width: _self.body.width(),
					height: _self.body.height(),
					"background-color": _self.color,
					opacity: _self.opacity,
					position: "absolute",
					top: 0,
					left: 0
				});
				_self.overlay.insertBefore(_self.body);				
			},			
			renderMsg: function(msg){
				var s = '';
				s += '<div class="dialog_message_container">';
					s += '<div class="dialog_header">';
						s += '<div class="dialog_title">'
							s += '<h2>' + _self.title + '</h2>'
						s += '</div>';
						s += '<div class="dialog_close">';
						s += '<img src="/images/ui/delete_icon.jpg" alt="x"/>';
						s += '</div>';
						s += '<div class="clr"></div>';
					s += '</div>';
					s += '<div class="dialog_body">';						
						s += msg;
					s += '</div>';
				s += '</div>';
				_self.msg_container = $(s).css({
					"z-index": ++_zindex,
					"box-shadow": "0 0 10px rgb(100,100,100)",
					display: "none",
					width: _self.width,
					"min-height": _self.min_height,
					position: "fixed",
					top: "10%",
					left: "50%",
					"background-color": "#FFF",
					"margin-left": (_self.width/2)*(-1)					
				});
				_self.msg_container.insertBefore(_self.body);
				if(_self.autoshow) {
					_self.show();
				}
				_self.msg_container.find('.dialog_close').click(_self.destroy);
			},
			show: function(){								
				_self.overlay.fadeIn(500);
				_self.msg_container.fadeIn(500);
				_self.onLoad();
			},
			onLoad: function() {},	
			doPost: function() {
				$.post(
					_self.url,
					$.extend({'ajax_request' : true}, _self.params),
					function(html) {
						_self.renderMsg(html);												
					}
				);
			},					
			destroy: function() {
				_self.msg_container.fadeOut(500,function(){
					_self.msg_container.remove();
				});				
				_self.overlay.fadeOut(500,function(){
					_self.overlay.remove();
				});				
			}
		};
		$.extend(_self,options);
		_self.init();
		return _self;
	}; 
})(jQuery);
