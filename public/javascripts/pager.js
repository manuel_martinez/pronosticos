/**
 * @author Manuel Martínez
 */
(function(){
	$.pager = function(options) {
		var _self = {
			url: '',
			not_found: 'No hay registros',
			loader_container: null,
			contents_container: $(''),
			nav_container: null,
			previous: null,
			next: null,
			total: 0,
			page: 0,
			params: {},
			per_page: 0,
			init: function(){				
				_self.initLoader();
				_self.initNav();
				_self.loadContents();
			},
			initLoader: function() {
				var s = '';
				s += '<div class="pager_loader">';
					s += '<img src="/images/ui/preloader.gif", alt="loading..." />';
				s += '</div>';
				_self.loader_container = $(s).css({
					width: _self.contents_container.width()
				});
				_self.loader_container.insertBefore(_self.contents_container).hide();				
			},
			initNav: function() {
				var s = '';
				s += '<div class="pager_nav">';
					s += '<span class="pager_button" id="pager_prev_btn">Previous</span>';
					s += '<span class="pager_button" id="pager_next_btn">Next</span>';					
				s += '</div>';
				_self.nav_container = $(s).css({
					width: _self.contents_container.width()
				});
				_self.next = _self.nav_container.find('#pager_next_btn');
				_self.previous = _self.nav_container.find('#pager_prev_btn');				
				_self.setNavActions();
				_self.setNav();
				_self.nav_container.insertAfter(_self.contents_container);
			},
			loadContents: function() {
				_self.loader_container.fadeIn(500);
				_self.contents_container.html('');
				_self.doPost();
			},
			doPost: function() {				
				$.post(
					_self.url,
					$.extend({'page': _self.page},_self.params),
					function(response) {
						_self.total = parseInt(response.total);
						_self.per_page = parseInt(response.per_page);
						_self.loader_container.hide();
						_self.setNav();
						if(_self.total > 0) {
							_self.onResponse(response.contents);	
						}
						else {
							var s = '<p>' + _self.not_found + '</p>';
							_self.contents_container.html(s).hide().fadeIn(500);	
						}												
					}
				);
			},
			onResponse: function(contents) {
				
			},
			setNav: function() {
				_self.previous.show();
				_self.next.show();
				if(_self.page == 0) {					
					_self.previous.hide();
				}
				if((_self.page + 1) * _self.per_page >= _self.total) {
					_self.next.hide();
				}				
			},
			setNavActions: function() {
				_self.next.click(function(e){
					e.preventDefault();
					if((_self.page + 1) * _self.per_page < _self.total) {
						++_self.page;
						_self.loadContents();
					}					
				});
				_self.previous.click(function(e){
					e.preventDefault();
					if(_self.page > 0) {
						--_self.page;
						_self.loadContents();
					}
				})
			}
		}
		$.extend(_self,options);
		_self.init();
		return _self;
	};
})(jQuery);
