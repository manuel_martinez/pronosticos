/**
 * @author Manuel Martínez
 */
(function(){
	$.search_bar = function(options){
		var _self = {
			placeholder: '',
			not_found: 'No results found',
			url: null,
			q: null,
			params: {},
			input: null,
			container: $(''),
			loader: null,
			results_container: null,
			default_timeout: 1,
			timeout: 0,
			searching: false,				
			init: function(){
				_self.initSearchBar();
				_self.initInput();
			},
			initInput: function(){
				setInterval(function(){
					if(_self.searching){
						if(_self.timeout == 0) {
							_self.resetSearching();
							_self.doPost();
						}
						else {
							--_self.timeout;
						}
					}
				}, 500);
				_self.input.keyup(function(e){
					_self.results_container.hide();
					var query = _self.input.val();
					if(_self.q == null || query != _self.q) {						
						if(query) {
							_self.q = query;							
							_self.setSearching();
						}
						else {
							_self.q = null;
							_self.resetSearching();
						}
					}
				});
			},
			initSearchBar: function() {
				var s = '';				
				var rc = '';
				s += '<div id="sb_container">';
				s += '<div id="sb_input_container">';
				s += '<input type="text" id="sb_input" autocomplete="off" placeholder="' + _self.placeholder + '"/>';				
				s += '</div>';
				s += '<div id="sb_loader"><img src="/images/ui/sb_default_preload.gif" alt="..." /></div>';								
				s += '</div">';					
				_self.container.html(s);
				if(_self.results_container == null) {
					rc = '<div id="sb_results_container"></div>';					
					_self.results_container = $(rc);
					_self.results_container.appendTo(_self.container.find('#sb_container'));										
				}
				_self.loader = _self.container.find('#sb_loader');
				_self.input = _self.container.find('#sb_input');
			},
			doPost: function(){				
				$.post(
					_self.url,
					$.extend({query: _self.q}, _self.params),
					function(data){
						if(data.length == 0) {							
							var s = '<div class="sb_result">' + _self.not_found + '</div>';
							_self.results_container.html(s).hide().fadeIn(200);
						}
						else {
							_self.onResponse(data);
						}
					}
				);
			},
			setSearching: function(){
				_self.searching = true;
				_self.resetTimeout();
				_self.loader.show();
			},
			resetSearching: function(){
				_self.searching = false;
				_self.results_container.hide();
				_self.loader.hide();
			},
			resetTimeout: function() {
				_self.timeout = _self.default_timeout; 
			},
			onResponse: function(data){
				
			}
		};
		$.extend(_self,options);
		_self.init();
		return _self;	
	}
	
})(jQuery);
