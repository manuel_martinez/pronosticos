/**
 * @author Manuel Martínez
 */
var months = [
	'Ene','Feb',
	'Mar','Abr',
	'May','Jun',
	'Jul','Ago',
	'Sep','Oct',
	'Nov','Dic'
];
function setTableActions(options) {	
	$('table.custom_table td.actions img').hide();
	$('table.custom_table tr').mouseover(function(e){
		$(this).find('td.actions img').show();	
	});
	$('table.custom_table tr').mouseout(function(e){
		$(this).find('td.actions img').hide();	
	});
	$('table.custom_table td.actions a.delete_btn').click(function(e){
		e.preventDefault();
		var which = $(this).attr('id');
		var s = '';
		s += '<div class="dialog_msg">';
			s += options.confirmation_message == null ? 'Element will be deleted' : options.confirmation_message;
		s += '</div>';
		s += '<div class="dialog_actions">';
			s += '<a href="#" id="dialog_delete_btn" class="blue_btn">Eliminar</a>';
			s += '<a href="#" id="dialog_cancel_btn" class="blue_btn">Cancelar</a>';
		s += '</div>';
		var confirmation_dialog = $.dialog({
			title: '¿Estás seguro?',
			msg: s,
			autoshow: false,
			onLoad: function(){								
				confirmation_dialog.msg_container.find('#dialog_delete_btn').click(function(e){
					e.preventDefault();
					url = options.url == null ? '#' : options.url;
					$.post(
						url,{
							id: which
						},
						function(response) {
							confirmation_dialog.destroy();
							if(options.callback != null) {
								options.callback(response);
							}
						}						
					);
				});
				confirmation_dialog.msg_container.find('#dialog_cancel_btn').click(function(e){
					e.preventDefault();
					confirmation_dialog.destroy();
				});
			}			
		});
		confirmation_dialog.show();
	});
}
function date_format(string) {
	var s = '';
	
	var fields = string.split('-');
	s = fields[2] + '/' + months[fields[1]-1] + '/' + fields[0];
	return s;				
}